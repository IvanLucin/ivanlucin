#include "Library.h"


vector<Book*> Library :: getBooks()
{
	return books; 
}

void Library :: setBooks(vector<Book*> pb) 
{
	this->books = pb; 
}

set<string> Library::getBooksByAuthor(string author)
{
	set<string> BbA;
	for (int i = 0; i < books.size(); i++)
	{
		if (books[i]->getAuthor() == author)
		{
			BbA.insert(books[i]->getTittle());
		}
	}
	return BbA;
}


double Library::Mbctr(string author)
{
	double br = 0;
	for (int i = 0; i < books.size(); i++)
	{
		if (books[i]->getAuthor() == author)
		{
			br += books[i]->getMB();
		}
	}
	return br;
}

set<string> Library::searchBooks(string srch)
{
	set<string> findBook;

	for (int i = 0; i < books.size(); i++)
	{
		if ((books[i]->getTittle()).find(srch) > 0 && (books[i]->getTittle()).find(srch) <= books[i]->getTittle().size())
		{
			findBook.insert(books[i]->getTittle());
		}
	}
	return findBook;
}

void Library::print(set<string> theBooks)
{
	for (auto it = theBooks.begin(); it != theBooks.end(); ++it)
	{
		cout << *it;
		cout << endl;
	}
}