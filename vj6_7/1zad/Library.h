#include "books.h"
#pragma once


class Library
{
public:
	vector <Book*> books;
	void setBooks(vector<Book*>);
	vector<Book*> getBooks();
	set<string> getBooksByAuthor(string);
	double Mbctr(string);
	set<string> searchBooks(string);
	void print(set<string>);
};
