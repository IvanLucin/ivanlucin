#include "HardCopyBook.h"
#include "EBook.h"
#include "Library.h"


int main()
{

	
	vector<Book*> books;
	Library lib;

	HardCopyBook book("Aldous Huxley", " Brave New World", 1982, 286);
	Book* a = &book;
	books.push_back(a);
	HardCopyBook book1("Douglas Adams", "The Hitchhiker's Guide to the Galaxy", 2000, 305);
	Book* b = &book1;
	books.push_back(b);
	HardCopyBook book2("Aldous Huxley", "Island", 1994, 336);
	Book* c = &book2;
	books.push_back(c);
	EBook book3("Simson Garfinkel", "The Unix-Haters Handbook", 2005, "txt", 3.5);
	Book* d = &book3;
	books.push_back(d);
	EBook book4("Shin Takahashi, Iroha Inoue", "The Manga Guide to Linear Algebra", 2008, "txt", 34.6);
	Book* e = &book4;
	books.push_back(e);
	EBook book5("Shin Takahashi, Iroha Inoue", "The Manga Guide to Physics", 2009, "txt", 49.7);
	Book* f = &book5;
	books.push_back(f);


	lib.setBooks(books);
	set<string> gBbA = lib.getBooksByAuthor("Shin Takahashi, Iroha Inoue");
	lib.print(gBbA);
	cout << endl;
	double brMB = lib.Mbctr("Simson Garfinkel");
	cout << brMB<<"MB";
	cout << endl;
	cout << endl;
	set<string> searchBar = lib.searchBooks("Guide");
	lib.print(searchBar);
	cout << endl;


}