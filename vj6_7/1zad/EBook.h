#include "books.h"
#pragma once


class EBook : public Book 
{
protected:
	string file;
	double MB=0;
public:
	EBook(string, string, int, string, double);
	void setFile(string);
	string getFile();
	void setMB(double);
	double getMB();
};