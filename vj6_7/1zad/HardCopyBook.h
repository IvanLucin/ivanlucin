#include "books.h"
#pragma once

class HardCopyBook : public Book 
{
	int Pages;
public:
	HardCopyBook();
	HardCopyBook(string, string, int, int);
	void setPage(int);
	double getMB();
};
