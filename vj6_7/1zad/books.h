#include <string>
#include <iostream>
#include <vector>
#include <set>
#pragma once
using namespace std;

class Book 
{
	protected:
		string author;
		string tittle;
		int year;
	public:
		Book();
		virtual ~Book();
		Book(string, string, int);
		virtual double getMB() = 0;
		string getAuthor();
		string getTittle();
		int getYear();
};

