#include "EBook.h"

EBook::EBook(string name, string tittle, int year, string fileN, double nMB)
	:Book(name, tittle, year)
{
	this->file = fileN;
	this->MB = nMB;
}

void EBook :: setFile(string str)
{
	this->file = str;
}

string EBook :: getFile() 
{
	return file; 
}

void EBook :: setMB(double nMB) 
{
	this->MB = nMB; 
}

double EBook :: getMB() 
{
	return MB; 
}