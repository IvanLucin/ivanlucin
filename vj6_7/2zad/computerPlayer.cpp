#include "computerPlayer.h"

void computerPlayer::setPName(string s)
{
	this->playerName = s;
}

string computerPlayer::getPName()
{
	return this->playerName;
}

void computerPlayer::setHand()
{
	playerHand.clear();
	vector<int> a = {1, 2, 3};
	vector<int> b = { 1,2,5 };

	random_shuffle(a.begin(), a.end());
	random_shuffle(b.begin(), b.end());

	if (a[0] == 1)
	{
		playerHand.push_back(b[0]);
		ctr = b[0];
	}
	if (a[0] == 2)
	{
		playerHand.push_back(b[0]);
		playerHand.push_back(b[1]);
		ctr = b[0] + b[1];
	}
	if (a[0] == 3)
	{
		playerHand.push_back(b[0]);
		playerHand.push_back(b[1]);
		playerHand.push_back(b[2]);
		ctr = b[0] + b[1] + b[2];
	}

	

}

vector<int> computerPlayer::getHand()
{
	return this->playerHand;
}

int computerPlayer::getCtr()
{
	return this->ctr;
}

void computerPlayer::setGuess()
{
	vector<int> computerGuesses;
	for (int i = 0; i < 33; i++)
	{
		 computerGuesses.push_back(i);
	}
	random_shuffle(computerGuesses.begin(), computerGuesses.end());
	this->playerguess = computerGuesses[1];
}

int computerPlayer::getGuess()
{
	return this->playerguess;
}


void computerPlayer::setPoints(int p)
{
	this->points = p;
}

int computerPlayer::getPoints()
{
	return this->points;
}

