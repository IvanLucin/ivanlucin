#pragma once
#include<iostream>
#include<vector>
#include<string>
#include<algorithm>

using namespace std;

class player
{
public:
	int points = 0;
	string playerName;
	int playerguess=0;
	vector<int>playerHand = {};
	int ctr=0;
	~player();
	virtual vector<int> getHand()=0;
	virtual void setPoints(int)=0;
	virtual int getPoints()=0;
	virtual int getGuess() = 0;
	virtual void setPName(string)=0;
	virtual string getPName() = 0;
	virtual int getCtr() = 0;

	



};
