#pragma once
#include"player.h"

class computerPlayer : public player
{
public:
	void setPName(string);
	string getPName();
	void setHand();
	vector<int> getHand();
	int getCtr();
	void setGuess();
	int getGuess();
	void setPoints(int);
	int getPoints();
};
