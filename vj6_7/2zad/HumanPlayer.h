#pragma once
#include"player.h"


class HumanPlayer : public player
{
public:
	void setPName(string);
	string getPName();
	void setHand(vector<int>);
	vector<int> getHand();
	int getCtr();
	void setGuess(int);
	int getGuess();
	void setPoints(int);
	int getPoints();
};
