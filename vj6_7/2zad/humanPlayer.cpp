#include"HumanPlayer.h"

void HumanPlayer::setPName(string name)
{
	this->playerName = name;
}

string HumanPlayer::getPName()
{
	return this->playerName;
}

void HumanPlayer::setHand(vector<int> h)
{
	this->playerHand.clear();
	vector<int> a;
	int c = 0;
	for (int i = 0; i < h.size(); i++)
	{
		if (h.size() > 3)
		{
			cout << "Smijete unit max 3 vrijednosti"<<endl;
			break;
		}
		if (h[i] != 1 && h[i] != 2 && h[i] != 5)
		{
			cout << "Smijete unosit samo vrijednosti velicine 1, 2 ili 5"<<endl;
			break;
		}
		a.push_back(h[i]);
		c += h[i];
	}
	this->playerHand = a;
	this->ctr = c;

}

vector<int> HumanPlayer::getHand()
{
	return this->playerHand;
}

int HumanPlayer::getCtr()
{
	return this->ctr;
}

void HumanPlayer::setGuess(int n)
{
	this->playerguess = n;
}

int HumanPlayer::getGuess()
{
	return this->playerguess;
}

void HumanPlayer::setPoints(int p)
{
	this->points = p;
}

int HumanPlayer::getPoints()
{
	return this->points;
}
