#pragma once
#include<iostream>

using namespace std;

class money
{
public:
	int kn=0;
	int lp=0;

public:
	money() {}
	money(int knp, int lpp) { this->kn = knp; this->lp = lpp; }
	money(int pkn) { this->kn = pkn; }
	~money() { cout <<" ~money" << endl; }
	money operator+(const money& a);
	money operator-(const money& b);
	money operator+=(const money& c);
	money operator-=(const money& d);
	friend ostream& operator<<(ostream& os, const money& e);
	operator double() { return (kn * 100. + lp) / 100; }
};
