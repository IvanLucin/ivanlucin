#include<iostream>
using namespace std;

class board
{
	int x;
	int y;
public:
	void print_xy() const;
	char **matrix;
	board() = default;
	board(int kx,int ky);
	board(const board& copy);
	~board();
	void alociraj();
	void sastavi();
	void oslobodi();
	void print() const;
	void draw_char(double px,double py);
	void draw_up_line(double px, double py);
	void draw_line(struct point p1,struct point p2);

};

struct point
{
	double xs;
	double ys;
	
};


