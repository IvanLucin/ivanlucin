#include"Board.h"

int main()
{
	int x = 20;
	int y = 25;
	int i, j;
	point p1,p2,p3,p4,p5;
	board a(x, y);
	board b = a;
	board c = a;
	p1.xs = 2; p1.ys = 18;
	p2.xs = 18; p2.ys = 2;
	p3.xs = 2; p3.ys = 19;
	p4.xs = 18; p4.ys = 9;
	p5.xs = 2; p5.ys = 12;
	a.alociraj();
	a.sastavi();
	a.draw_char(p5.xs, p5.ys);
	a.print();
	a.oslobodi();
	b.alociraj();
	b.sastavi();
	b.draw_up_line(p4.xs, p4.ys);
	b.print();
	b.oslobodi();
	c.alociraj();
	c.sastavi();
	c.draw_line(p2,p1);
	c.print();
	c.oslobodi();



}


board::board(int kx, int ky)
{
	this->x = kx;
	this->y = ky;
}
board::~board()
{

}
board::board(const board& copy)
{
	this->x = copy.x;
	this->y = copy.y;
}

void board::print_xy() const
{
	cout << x << endl;
	cout << y << endl;
}


void board::alociraj()
{
	int row = x;
	int col = y;

	matrix = new char* [row];
	for (int i = 0; i < row; i++)
		matrix[i] = new char[col];
}

void board::sastavi()
{
	int row = x;
	int col = y;

	matrix[0][0] = 'x';
	matrix[0][col - 1] = 'x';
	matrix[row - 1][0] = 'x';
	matrix[row - 1][col - 1] = 'x';


	for (int j = 1; j < col - 1; j++)
	{
		matrix[0][j] = '-';
	}
	for (int j = 1; j < col - 1; j++)
	{
		matrix[row - 1][j] = '-';
	}
	for (int i = 1; i < row - 1; i++)
	{
		matrix[i][0] = '|';
	}
	for (int i = 1; i < row - 1; i++)
	{
		matrix[i][col - 1] = '|';
	}
	for (int i = 1; i < row - 1; i++)
		for (int j = 1; j < col - 1; j++)
		{
			matrix[i][j] = ' ';
		}


}
void board::oslobodi()
{
	int row = x;

	for (int i = 0; i < row; i++)
		delete[] matrix[i];
	delete[] matrix;

}
void board::print() const
{
	int row = x;
	int col = y;
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			cout << matrix[i][j];
		}
		cout << endl;
	}
}
void board::draw_char(double px, double py)
{
	int row=px, col=py;
	matrix[row][col] = '*';

}
void board::draw_up_line(double px, double py)
{
	int row = px, col = py;
	int j = col;
	for (int i = row; i > 0; i--)
	{
		if (j > 0)
		{
			matrix[i][j] = '*';
			j--;
		}

	}
}

void board::draw_line(struct point p1,struct point p2)
{
	 int row1 = p1.xs;
	 int col1 = p1.ys;
	 int row2 = p2.xs;
	 int col2 = p2.ys;
	int i, j;

	if (row1 == row2)
	{
		if (col1 < col2)
		{
			for (j = col1; j <= col2; j++)
			{
				matrix[row1][j] = '*';
			}
		}
		else
		{
			for (j = col2; j <= col1; j++)
			{
				matrix[row1][j] = '*';
			}
		}
	}
	if (row1 > row2)
	{
		if (col1 < col2)
		{
			int rr = row1 - row2;
			int cr = col2 - col1;
			if (rr == cr)
			{
				j = col2;
				for (i = row2; i <= row1; i++)
				{
					matrix[i][j] = '*';
					j--;
				}
			}
			else
				cout << "Nemoguce napravit ravnu crtu.";
		}
		if (col1 > col2)
		{
			int rr = row1 - row2;
			int cr = col1 - col2;
			if (rr == cr)
			{
				j = col1;
				for (i = row2; i <= row1; i++)
				{
					matrix[i][j] = '*';
					j--;
				}
			}
			else
				cout << "Nemoguce napravit ravnu crtu.";

		}
		if (col1 == col2)
		{
			for (i = row2; i <= row1; i++)
			{
				matrix[i][col1] = '*';
			}
		}

	}
	if (row1 < row2)
	{
		if (col1 < col2)
		{
			int rr = row2 - row1;
			int cr = col2 - col1;
			if (rr == cr)
			{
				j = col1;
				for (i = row1; i <= row2; i++)
				{
					matrix[i][j] = '*';
					j++;
				}
			}
			else
				cout << "Nemoguce napravit ravnu crtu.";
		}
		if (col1 > col2)
		{
			int rr = row2 - row1;
			int cr = col1 - col2;
			if (rr == cr)
			{
				j = col1;
				for (i = row1; i <= row2; i++)
				{
					matrix[i][j] = '*';
					j--;
				}
			}
			else
				cout << "Nemoguce napravit ravnu crtu.";
		}
		if (col1 == col2)
		{
			for (i = row1; i <= row2; i++)
			{
				matrix[i][col1] = '*';
			}
		}
	}
}

