#include "mac.h"
class igraci
{
	int bodovi = 0;
	int broj_igraca;
	string ime;

public:
	igraci();
	~igraci();
	void set_bodovi(int b);
	int get_bodovi(void);
	void set_br_igraca(int br);
	int get_br_igraca(void);
	void set_ime(string i);
	string get_ime(void);
	vector<karta> ruka;
	void pokazi_ruku()const;
	void punti(vector<karta> ruka);
	void print() const;
	
};
