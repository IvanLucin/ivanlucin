#pragma once
#include<iostream>
#include<vector>

using namespace std;



template<typename T>
void sort(T* c,int s)
{
	
	T temp;
	for (int i = 0; i < s; i++)
	{
		for (int j = i + 1; j < s; j++)
		{
			if (c[j] < c[i])
			{
				temp = c[i];
				c[i] = c[j];
				c[j] = temp;
			}
		}
	}

}

template<>
void sort(char* c,int s)
{
	char temp;
	for (int i = 0; i < s; i++)
	{
		for (int j = i + 1; j < s; j++)
		{
			if (tolower(c[j]) < tolower(c[i]))
			{
				temp = c[i];
				c[i] = c[j];
				c[j] = temp;
			}
		}
	}
}

template<typename V>
class Stack
{
public:
	vector<V> vek;
	Stack(vector<V> v) { this->vek = v; }
	~Stack() { cout << "destruktor" << endl; }
	void push(V);
	void pop();
	bool is_empty();
	void print();
};

template<typename V>
void Stack<V>::push(V a)
{
	vek.push_back(a);
}

template<typename V>
void Stack<V>::pop()
{
	vek.pop_back();
}

template<typename V>
bool Stack<V>::is_empty()
{
	if (vek.size() == 0)
		return true;
	else
		return false;
}

template<typename V>
void Stack<V>::print()
{
	for (int i = 0; i < vek.size(); i++)
	{
		cout << vek[i] << ",";
	}
}