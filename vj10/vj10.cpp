#include <iostream>
#include<vector>
#include<iterator>
#include<fstream>
#include<algorithm>

using namespace std;

bool lessthan(int n) { return n < 300; }
struct comp
{
    bool operator() (int a, int b)
    {
        return a > b;
    }
};
int main()
{
    vector<int> v;
    ifstream f("brojevi.txt");
    if (!f.is_open())
        return 1;
    istream_iterator<int> is(f), eos;
    copy(is, eos, back_inserter(v));
    ostream_iterator<int> os(cout, ",");
    copy(v.begin(), v.end(), os);
    int sum = 0;
    for (int i = 0; i < v.size(); i++)
    {
        if (v[i] > 500)
            sum += v[i];
    }
    cout << endl << sum << endl;

    cout << "min element is:" << *min_element(v.begin(), v.end())<<endl;
    cout << "max element is:" << *max_element(v.begin(), v.end())<<endl;
    vector<int>::iterator k;
    k=remove_if(v.begin(), v.end(), lessthan);
    for (vector<int>::iterator it = v.begin(); it != k; it++)
    {
        cout << " " << *it;
    }
    cout << endl << endl;
    sort(v.begin(), v.end(), comp());
    copy(v.begin(), v.end(), os);
}


