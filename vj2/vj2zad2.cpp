#include<iostream>
#include<cmath>
using namespace std;

struct kruznica
{
	int r;
	int x;
	int y;
};
int funkcija(const kruznica (&a)[3], const kruznica &b)
{
	int i;
	int br=0;

	for (i = 0; i < 3; i++)
	{
		if (sqrt(pow(a[i].x - b.x, 2) + pow(a[i].y - b.y, 2)) < (a[i].r + b.r))
		{
			br++;
		}
	}
	return br;
}

int main()
{
	kruznica a[3];
	int i;
	cout << " Unesite radius te koordinate za x i y osi za 3 kruznice: ";

	for (i = 0; i < 3; i++)
	{
		cin >> a[i].r;
		cin >> a[i].x;
		cin >> a[i].y;
	}

	kruznica b;
	cout << "Unesite radius i osi x i y za svoju kruznicu: ";

	cin >> b.r;
	cin >> b.x;
	cin >> b.y;

	int krizanja = funkcija(a,b);

	cout << "sijeku se njih: " << krizanja;
}