#include <iostream>
using namespace std;
void trazi(int n, int niz[], int& min, int& max)
{
    int i;
    for (i = 0; i < n; i++)
    {
        if (niz[i] < min)
        {
            min = niz[i];
        }
        if (niz[i] > max)
        {
            max = niz[i];
        }
    }
}
int main()
{
    int min, max;
    int niz[] = { 1,5,4,3,7,12,34,8,22,11 };
    int n = sizeof(niz) / sizeof(niz[0]);
    min = max = niz[1];

    trazi(n, niz, min, max);
    cout << "Minimalna vrijednost je:" << min << endl;
    cout << "Maksimalna vrijednost je:" << max << endl;
}

