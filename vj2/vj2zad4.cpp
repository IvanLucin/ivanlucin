#include <iostream>
using namespace std;

void prosirenje(int* niz, int n)
{
	for (int i = 2; i < n; i++)
	{
		niz[i] = niz[i - 1] + niz[i - 2];
	}
}
int main()
{
	int n, i;
	cout << "Unesite velicinu niza:";
	cin >> n;
	int* niz;
	niz = new int[n];
	niz[0] = 1;
	niz[1] = 1;
	prosirenje(niz, n);

	for (i = 0; i < n; i++)
	{
		cout << niz[i] << ",";
	}
	delete[] niz;
	niz = 0;
}